// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cryptctl

import (
	"github.com/pkg/errors"
	"io/ioutil"
)

type DefaultLuks struct {
	options map[string]string
}

func NewDefaultLuks(authOptions []AuthOption) (LuksAuth, error) {
	var defaultAuth DefaultLuks
	valid := false
	for _, opt := range authOptions {
		err := defaultAuth.SetOption(opt.Key, opt.Value)
		if err != nil {
			return defaultAuth, err
		}

		if opt.Key == "path" {
			valid = true
		}
	}

	if ! valid {
		return defaultAuth, errors.New("no path for default encryption key provided")
	}

	return defaultAuth, nil
}

func (d DefaultLuks) GetKey() ([]byte, error) {
	productUuid, err := ioutil.ReadFile(d.options["path"])
	if err != nil {
		return []byte{}, err
	}

	return productUuid, nil
}

func (d DefaultLuks) SetOption(key string, value string) error {
	if key != "path" {
		return errors.Errorf("invalid option passed to default auth: [%v]:[%v]", key, value)
	}

	return nil
}
