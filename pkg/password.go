// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cryptctl

import (
	"golang.org/x/crypto/ssh/terminal"
	"syscall"
	"github.com/pkg/errors"
)

type PasswordLuks struct {
	options map[string]string
}

func NewPasswordLuks(authOptions []AuthOption) (LuksAuth, error) {
	var passwordAuth PasswordLuks

	for _, opt := range(authOptions) {
		err := passwordAuth.SetOption(opt.Key, opt.Value)
		if err != nil {
			return passwordAuth, err
		}
	}

	return passwordAuth, nil
}

func (p PasswordLuks) GetKey() ([]byte, error) {
	key, err := terminal.ReadPassword(syscall.Stdin)

	if err != nil {
		return []byte{}, errors.WithMessage(err, "ReadPassword err: %v\n")
	}

	return key, nil
}

func (p PasswordLuks) SetOption(key string, value string) error {
	switch key {
	case "key":
	case "key_type":
		p.options[key] = value
	default:
		return errors.Errorf("invalid option for password authentication: %v : %v", key, value)
	}

	return nil
}
