// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cryptctl

import (
	"golang.org/x/crypto/ssh/terminal"
	"os/exec"
	"syscall"
	"github.com/pkg/errors"
)

type YubikeyLuks struct {
	options map[string]string
}

func NewYubikeyLuks(authOptions []AuthOption) (LuksAuth, error) {
	var yubikeyAuth YubikeyLuks

	for _, opt := range(authOptions) {
		yubikeyAuth.SetOption(opt.Key, opt.Value)
	}

	return yubikeyAuth, nil
}

func (YubikeyLuks) GetKey() ([]byte, error) {
	key, err := terminal.ReadPassword(syscall.Stdin)

	if err != nil {
		return []byte{}, errors.WithMessage(err, "ReadPassword error.\n")
	}

	challengeResponse := exec.Command("ykchalresp", "-2", "-i-")
	stdin, err := challengeResponse.StdinPipe()
	if err != nil {
		return []byte{}, errors.WithMessage(err, "Failed to get stdin for Yubikey challenge-response.\n")
	}

	go func() {
		defer stdin.Close()
		_, _ = stdin.Write(key)
	}()

	response, err := challengeResponse.CombinedOutput()
	if err != nil {
		return []byte{}, errors.WithMessage(err, "Failed to recieve repsonse to challenge.\n")
	}

	return response, nil
}

func (p YubikeyLuks) SetOption(key string, value string) error {
	return errors.Errorf("Invalid option: %v", key)
}
